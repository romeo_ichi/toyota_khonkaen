$(window).scroll(function () {
    if ($(window).scrollTop() > 123) {
        $('header .desktop-header').addClass('fixed-header');
		
    }else{
        $('header .desktop-header').removeClass('fixed-header');
    }
});
/*if($(window).width() <= 640) {
	$("img.replace").each(function() {
	  $(this).attr("src", $(this).attr("src").replace("assets/images/", "assets/images/mobile/"));
	});
	$("#slider img").each(function() {
	  $(this).attr("src", $(this).attr("src").replace("assets/images/", "assets/images/mobile/"));
	});
}*/
includeHTML();
function includeHTML() {
  var z, i, elmnt, file, xhttp;
  /*loop through a collection of all HTML elements:*/
  z = document.getElementsByTagName("*");
  for (i = 0; i < z.length; i++) {
    elmnt = z[i];
    /*search for elements with a certain atrribute:*/
    file = elmnt.getAttribute("w3-include-html");
    if (file) {
      /*make an HTTP request using the attribute value as the file name:*/
      xhttp = new XMLHttpRequest();
      xhttp.onreadystatechange = function() {
        if (this.readyState == 4) {
          if (this.status == 200) {elmnt.innerHTML = this.responseText;}
          if (this.status == 404) {elmnt.innerHTML = "Page not found.";}
          /*remove the attribute, and call this function once more:*/
          elmnt.removeAttribute("w3-include-html");
          includeHTML();
        }
      }      
      xhttp.open("GET", file, true);
      xhttp.send();
      /*exit the function:*/
      return;
    }
  }
}

$(document).ready(function() {
	// Javascript to enable link to tab
	/*var url = document.location.toString();
	if (url.match('#')) {
		$('.nav-pills a[href="#' + url.split('#')[1] + '"]').tab('show');
		$('.nav a[href="#' + url.split('#')[1] + '"]').tab('show');
	} 

	// Change hash for page-reload
	$('.nav-pills a').on('shown.bs.tab', function () {
		//window.location.hash = e.target.hash;
		$('html, body').animate({
		scrollTop: $($.attr(this, 'href')).offset().top-600
	}, 500);
	})
	$('.nav a').on('shown.bs.tab', function () {
		//window.location.hash = e.target.hash;
		$('html, body').animate({
		scrollTop: $($.attr(this, 'href')).offset().top-600
	}, 500);
	})
	$(".nav a[href^='#']").click(function(e) {
		e.preventDefault();
		if (url.match('#')) {
			$('.nav a[href="#' + url.split('#')[1] + '"]');
		}

		var position1 = $($(this).attr("href")).offset().top-500;

		$("body, html").animate({
			scrollTop: position1
		}  );
	});*/
	
	$('.custom-dropdown').hover(function(){
		$('.dropdown-toggle').removeClass('show');
		$('.dropdown-menu').removeClass('show');
	});
	$('.custom-dropdown').click(function(){
		$(this).children('.dropdown-toggle').toggleClass('show');
		$(this).children('.dropdown-menu').toggleClass('show');
	});
	
    $("input[name$='customRadio']").click(function() {
        var test = $(this).val();

        $("#showdoc2").hide();
        $("#showdoc" + test).show();
    });
	
	$("input[name$='customRadioSub']").click(function() {
        var test1 = $(this).val();

        $("#showdoc3").hide();
        $("#showdoc" + test1).css("display","inline-block");
    });
	
	$('#loading').hide();
	$("#loadparts").click(function(){
		$('#loading').show();
		$.ajax({
			url: 'include/loadparts.html',
			dataType: 'html',
			success: function(html) {
				$('#load-parts').append(html);
				$('#loading').hide();
			}
		});
	});
	$('.price-table').hide();
	$('#showprice').click(function(){
		$('.price-table').show();
	});
	$('#hideprice').click(function(){
		$('.price-table').hide();
	});
	$('.cookies-fix .btn').click(function(){
		$('.cookies-fix').hide();
	});
	
	$('.car-color .btn').click(function(){
		$('.car-color .btn').removeClass('active');
		$(this).addClass('active');
	});
});

setTimeout(function(){
	$('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function(){
		$(this).toggleClass('open');
	});
	$('#nav-icon4').click(function(){
		$('.mobile-menu').toggleClass('open');
		$('body').toggleClass('lock');
	});
	$('.drop').click(function(){
		$(this).toggleClass('show');
	});
	
}, 1000);



/* Video Popup*/
jQuery(document).ready(function ($) {
  // Define App Namespace
  var popup = {
    // Initializer
    init: function() {
      popup.popupVideo();
    },
    popupVideo : function() {

    $('.video_model').magnificPopup({
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: false,
    gallery: {
          enabled:true
        }
  });

/* Image Popup*/ 
 $('.gallery_container').magnificPopup({
    delegate: 'a',
    type: 'image',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: false,
    gallery: {
          enabled:true
        }
  });

    }
  };
  popup.init($);
});

